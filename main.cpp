#include "test/test.hpp"
#include "console_ui.hpp"

int main()
{
    test();
    console_ui<int> ui;
    ui.execute();
    return 0;
}