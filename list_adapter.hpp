#ifndef INC_201701_LIST_ADAPTER_HPP
#define INC_201701_LIST_ADAPTER_HPP

#include <optional>
#include <istream>
#include <ostream>
#include <functional>
#include "linear_list.hpp"

#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR -2

template <typename T>
class list_adapter
{
    std::optional<linear_list<T>> list;  //给列表增加"列表不存在"的状态.
public:
    auto InitalList()
    {
        if (list)
            return ERROR;
        list = linear_list<T>();
        return OK;
    }

    auto DestroyList()
    {
        if (!list)
            return ERROR;
        list.reset();
        return OK;
    }

    auto ClearList()
    {
        if (!list)
            return ERROR;
        list->clear();
        return OK;
    }

    int ListEmpty() const noexcept
    {
        if (!list)
            return ERROR;
        else
            return list->is_empty();
    }

    int ListLength() const noexcept
    {
        if (!list)
            return ERROR;
        return list->size();
    }

    auto GetElem(std::size_t index, T &result) const
    {
        if (!list)
            return ERROR;
        if (index < 1 || index > list->size())
            return ERROR;
        result = (*list)[index - 1];
        return OK;
    }

    template <typename Equal>
    int LocateElem(T const &e, Equal compare) const
    {
        using namespace std::placeholders;
        if (!list)
            return 0;
        auto pos = list->find(std::bind(compare, e, _1));
        if (pos == list->size())
            return 0;
        return pos + 1;
    }

    auto PriorElem(T const &cur, T &prev) const
    {
        if (!list)
            return ERROR;
        auto pos = list->find([&cur](auto e)
                              { return cur == e; });
        if (pos == 0)
            return ERROR;
        else if (pos == list->size())
            return ERROR;
        else
        {
            prev = (*list)[pos - 1];
            return OK;
        }
    }

    auto NextElem(T const &cur, T &next) const
    {
        if (!list)
            return ERROR;
        auto pos = list->find([&cur](auto e)
                              { return cur == e; });
        if (pos == list->size() - 1)
            return ERROR;
        else if (pos == list->size())
            return ERROR;
        else
        {
            next = (*list)[pos + 1];
            return OK;
        }
    }

    auto ListInsert(std::size_t index, T const &e)
    {
        if (!list)
            return ERROR;
        if (index < 1 || index > list->size() + 1)
            return ERROR;
        list->insert(index - 1, e);
        return OK;
    }

    auto ListDelete(std::size_t index, T &result)
    {
        if (!list || ListEmpty())
            return ERROR;
        if (index < 1 || index > list->size())
            return ERROR;
        result = std::move((*list)[index - 1]);
        list->erase(index - 1);
        return OK;
    }

    template <typename Callable>
    auto ListTraverse(Callable callable)
    {
        if (!list)
            return ERROR;
        list->iterate(callable);
        return OK;
    }

    friend std::ostream &operator<<(std::ostream &out, list_adapter<T> const &list)
    {
        if (list.list)
            out << 1 << " " << *list.list;
        else
            out << 0 << " ";
        return out;
    }

    friend std::istream &operator>>(std::istream &in, list_adapter<T> &list)
    {
        list.list = linear_list<T>();
        int exists;
        in >> exists;
        if (exists)
        {
            in >> *list.list;
        }
        else
            list.list.reset();
        return in;
    }
};


#endif //INC_201701_LIST_ADAPTER_HPP
