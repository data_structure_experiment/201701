#include <vector>
#include <string>
#include "test_list.hpp"
#include "../linear_list.hpp"

namespace
{
    template <typename T>
    void test_size(linear_list<T> const &list, size_t size)
    {
        assert(list.size() == size);
        assert(list.end() - list.begin() == (std::ptrdiff_t)size);
    }

    template <typename U, typename T>
    void test_equal(linear_list<T> const &list, std::initializer_list<U> i)
    {
        bool result = (list == linear_list<std::string>(i));
        assert(result);
    }
}

void test_list()
{
    {
        linear_list<std::string> list(5);
        test_size(list, 5);
        list = {"0", "1", "3", "4"};
        test_size(list, 4);
        list.insert(2, "2");
        test_size(list, 5);
        test_equal(list, {"0", "1", "2", "3", "4"});
        list.insert(5, "5");
        test_size(list, 6);
        test_equal(list, {"0", "1", "2", "3", "4", "5"});
        list.erase(1);
        test_size(list, 5);
        test_equal(list, {"0", "2", "3", "4", "5"});
        {
            size_t pos = list.find([](auto const &s)
                                   { return s == "2"; });
            assert(pos == 1);
        }
        {
            using vector_t = std::vector<std::string>;
            vector_t vec;
            list.iterate([&](auto const &s)
                         { vec.push_back(s); });
            bool equal = (vec == vector_t{"0", "2", "3", "4", "5"});
            assert(equal);
        }
        list.clear();
        test_size(list, 0);
        assert(list.is_empty());
        test_equal<char *>(list, {});
    }

}
