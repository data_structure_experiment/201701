#ifndef INC_201701_SAVE_LOAD_HPP
#define INC_201701_SAVE_LOAD_HPP

#include <istream>
#include <ostream>
#include "linear_list.hpp"

template <typename T>
std::istream& operator>>(std::istream& in, linear_list<T>& list)
{
    list.clear();
    std::size_t size;
    in >> size;
    for (std::size_t i = 0; i < size; ++i)
    {
        T value;
        in >> value;
        list.insert(list.size(), std::move(value));
    }
    return in;
}
template <typename T>
std::ostream& operator<<(std::ostream& out, linear_list<T> const& list)
{
    out << list.size() << " ";
    for (auto const& each : list)
    {
        out << each << " ";
    }
    out << "\n";
    return out;
}

#endif //INC_201701_SAVE_LOAD_HPP
