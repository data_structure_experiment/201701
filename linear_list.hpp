#ifndef INC_201701_LINEAR_LIST_HPP
#define INC_201701_LINEAR_LIST_HPP

#include <algorithm>
#include <cassert>
#include <memory>

template <typename T>
class linear_list //线性表的实现.
{
public:
    using value_type = T; //元素类型
    using array_type = value_type[]; //存储元素的数组类型
    using handler_type = std::unique_ptr<array_type>; //管理内存的智能指针.
    using size_type = std::size_t; //可以用来储存索引和列表长度的类型.

    linear_list() //默认构造函数,将列表初始化为长度为0的表即空表.
        : linear_list(0)
    {}

    explicit linear_list(size_type size) //将表初始化为长度为 size 的表
        : handler(new value_type[size]{}), size_(size), capacity(size)
    {}

    template <typename U>
    linear_list(std::initializer_list<U> list) //以初始化列表 list 中的元素初始化该表
        : handler(new T[list.size()]), size_(list.size()), capacity(list.size())
    {
        //        std::move(list.begin(), list.end(), &handler[0]); //用STL更简洁,出于实验考虑手工写循环.
        auto dest = begin();
        for (auto src = list.begin(); src != list.end(); ++src, ++dest)
            *dest = *src;
    }

    ~linear_list() = default;

    linear_list(linear_list &&src) = default; //默认移动构造函数
    linear_list(linear_list const &src) //拷贝构造函数
        : handler(new T[src.size()]), size_(src.size()), capacity(src.size())
    {
        //        std::copy(src.begin(), src.end(), begin());       //同上
        auto dest = begin();
        for (auto src_begin = src.begin(); src_begin != src.end(); ++src_begin, ++dest)
            *dest = *src_begin;
    }

    linear_list &operator=(linear_list &&src) = default; //默认移动赋值运算符
    auto &operator=(linear_list const &src) //拷贝赋值运算符
    {
        *this = linear_list(src); //通过拷贝构造临时对象并将其移动给自己,以达到赋值目的.
        return *this;
    }

    void reset(size_type size) //将表重新设为长度为 size 的列表.
    {
        *this = linear_list(size);
    }

    void clear() //清空表, 变为空表.
    {
        *this = linear_list();
    }

    auto is_empty() const noexcept //判断表是否为空
    {
        return size_ == 0;
    }

    auto size() const noexcept //获取表长度.
    {
        return size_;
    }

    auto &operator[](size_type index) noexcept //获取表中索引为 index 的元素, 索引从0开始.
    {
        return handler[index];
    }

    auto const &operator[](size_type index) const noexcept //同上,只读重载版本
    {
        return handler[index];
    }

    template <typename pred>
    size_type find(pred p) const noexcept(noexcept(p(std::declval<value_type>())))  //在列表中,对于每一个元素e, 返回第一个满足bool(p(e))为true的元素的索引.
    {
        //                return std::find_if(handler.get(), handler.get() + size_, p) - begin();
        for (auto iter = begin(); iter != end(); ++iter)
        {
            if (p(*iter))
                return iter - begin();
        }
        return end() - begin();
    }

    template <typename U>
    void insert(size_type pos, U &&u) //在列表中索引为pos的位置插入u.
    {
        assert(pos <= size_);
        if (size_ == capacity)
            expand(pos);
        else
            move_forward_one(pos);
        handler[pos] = std::forward<U>(u);
        ++size_;
    }

    void erase(size_type pos) noexcept //移除列表中索引为pos的元素.
    {
        assert(pos < size_);
        move_backward_one(pos);
        --size_;
    }

    template <typename Visit>
    void iterate(Visit visit) noexcept(noexcept(visit(std::declval<value_type>()))) //对列表中的每一个元素e, 调用visit(e).
    {
        for (auto iter = begin(); iter != end(); ++iter)
            visit(*iter);
    }

    value_type *begin() noexcept //以下6个函数为迭代器.
    {
        return handler.get();
    }

    value_type const *begin() const noexcept
    {
        return handler.get();
    }

    value_type const *cbegin() const noexcept
    {
        return begin();
    }

    value_type *end() noexcept
    {
        return handler.get() + size_;
    }

    value_type const *end() const noexcept
    {
        return handler.get() + size_;
    }

    value_type const *cend() const noexcept
    {
        return end();
    }

    template <typename U>
    friend auto operator==(linear_list<T> const &lhs, linear_list<U> const &rhs) //判断列表lhs与rhs相等.对于两个列表中的元素a和b, 使用std::equal_to<void>(a, b)判断元素相等性.
    {
        if (lhs.size() != rhs.size())
            return false;
        for (size_type i = 0; i < lhs.size(); ++i)
        {
            if (!std::equal_to<void>()(lhs[i], rhs[i]))
                return false;
        }
        return true;
    }

private:
    void expand(size_type interval) //扩展列表的空间, 并将索引大于等于interval的所有元素向索增加方向移动一位以将interval处空出.
    {
        assert(size_ == capacity);
        auto new_capacity = expanded_capacity();
        handler_type new_region(new T[new_capacity]);
        //        std::move(begin(), end(), new_region.get());
        for (size_type src = 0, dst = 0; src < size_; ++src, ++dst)
        {
            if (dst == interval)
                ++dst;
            new_region[dst] = std::move(handler[src]);
        }
        handler = std::move(new_region);
        capacity = new_capacity;
    }

    auto expanded_capacity() const //获取列表空间扩展后的应有大小.
    {
        if (size_ <= 1)
            return size_ + 1;
        else
            return size_type(size_ * 1.8);
    }

    void move_forward_one(size_type begin_pos) noexcept //将列表中索引大于等于begin_pos的所有元素向索引增加的方向移动一个位置.
    {
        assert(size_ < capacity);
        assert(begin_pos <= size_);
        for (auto next = size_; next != begin_pos; --next)
        {
            handler[next] = std::move(handler[next - 1]);
        }
    }

    void move_backward_one(size_type begin_pos) noexcept //将列表中索引大于等于begin_pos + 1的所有元素向索引减小方向移动一个位置, 以将原来索引为begin_pos的元素覆盖掉.
    {
        assert(size_ > 0);
        assert(begin_pos < size_);
        for (auto prev = begin_pos; prev != size_ - 1; ++prev)
        {
            handler[prev] = std::move(handler[prev + 1]);
        }
    }

    handler_type handler;
    size_type size_;
    size_type capacity;
};

#endif //INC_201701_LINEAR_LIST_HPP
